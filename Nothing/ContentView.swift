//
//  ContentView.swift
//  Nothing
//
//  Created by Ronaldo Nascimento on 1/23/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text("Corporations")
            Button("Argent") {
            }
            Button("Cosmog Cogs") {
            }
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

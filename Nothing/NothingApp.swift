//
//  NothingApp.swift
//  Nothing
//
//  Created by Ronaldo Nascimento on 1/23/23.
//

import SwiftUI

@main
struct NothingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
